<?php
require_once('../config/config.php');

class DBManager extends PDO
{
    public $server = SERVER;
    public $user = USER;
    public $pass = PASS;
    public $port = PORT;
    public $db = DB;

    private $connection;

    public function __construct(){
        $this -> conectar();
    }

    private final function conectar(){
        $connection = null;

        try {
            if (is_array(PDO::getAvailableDrivers())){
                if (in_array("psql", PDO::getAvailableDrivers())){
                    $connection = new PDO($this->server, $this->user, $this->pass, $this->port, $this->db);
                }else{
                    throw new PDOException("Error");
                }
            }
        }catch (PDOException $e){
            echo $e->getMessage();
        }
        $this->setConnection($connection);
    }

    public final function setConnection($connection){
        $this->connection = $connection;
    }

    public final function getConnection(){
        return $this->connection;
    }

    public final function closeConnection(){
        $this->setConnection(null);
    }


}