<?php


function validateLogin(){

}

function validaEdad(){
    $fechaNacimiento= new DateTime($_POST['birthdate']);
    $dataActual = date("d-m-y");
    $dataActual = new DateTime($dataActual);
    $calcul = $fechaNacimiento->diff($dataActual);
    $edad = $calcul->y;

    if ($edad >= 18) {
        return true;
    } else {
        return false;
    }
}

function validaNom(){
    if (ctype_alpha(str_replace(' ', '', $_POST['name']))) {
        return true;
    }
    return false;
}

function validaApellido(){
    if (ctype_alpha(str_replace(' ', '',$_POST['apellido']))) {
        return true;
    }
    return false;
}

function validaDNI(){
    if($_POST['dni']){
        $letra = substr($_POST['dni'], -1);
        $numeros = substr($_POST['dni'], 0, -1);
        if ( substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros%23, 1) == $letra && strlen($letra) == 1 && strlen ($numeros) == 8 ){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}


function validaNumeroMobil(){
    $longitudNumero = strlen($_POST['mobilePhone']);

    if ($longitudNumero > 9){
        return false;
    }else{
        if ($longitudNumero[1] == '6' || $longitudNumero[1] == '7'){
            return true;
        }else{
            return false;
        }
        return true;
    }
}

function validaEmail(){
    $email = $_POST['email'];

    if($email){
        return (false !== filter_var($email, FILTER_VALIDATE_EMAIL));
    }else{
        return false;
    }
}

function validaContrasenya(){
    $contrasenya = $_POST['password'];
    $contraseyaDos = $_POST['repeatPassword'];

    if (strlen($contrasenya) >= '8' && preg_match('`[a-z]`', $contrasenya) && preg_match('`[A-Z]`', $contrasenya) &&  preg_match('`[0-9]`', $contrasenya)){
        return true;
    }else if (!$contrasenya === $contraseyaDos){
        echo "Tu contrasenya no coicide";
        return false;
    }
    return false;
}


function validateRegister(){

    $cumpleCondicion = true;
    $mensajeError="";

    if (!validaNom()) {
        $_POST['name'] = '';
        $mensajeError  .= " ***El campo nombre solo puede letras y espacios*** ".'</br>';
        $cumpleCondicion = false;
    }

    if (!validaApellido()) {
        $_POST['apellido'] = '';
        $mensajeError .= "*** El campo apellido solo puede letras y espacios*** ".'</br>';
        $cumpleCondicion = false;
    }

    if(!validaEdad()){
        $_POST['birthdate'] = '';
        $mensajeError .= "*** No puedes registrarte, eres menor de edad*** ".'</br>';
        $cumpleCondicion = false;
    }

    if(!validaNumeroMobil()){
        $_POST['mobilePhone'] = '';
        $mensajeError .= "*** El campo telefono es incorrecto*** ".'</br>' ;
        $cumpleCondicion = false;
    }

    if(!validaDNI()){
        $_POST['dni'] = '';
        $mensajeError .= "*** El DNI esta mal escrito*** ".'</br>';
        $cumpleCondicion = false;
    }

    if(!validaEmail()){
        $_POST['email'] = '';
        $mensajeError .= "*** El email es incorrecto*** ".'</br>';
        $cumpleCondicion = false;
    }

    if(!validaContrasenya()){
        $_POST['password'] = '';
        $mensajeError .= "*** La contraseña tiene que contener mayúsculas, minúsculas, números, un carácter especial y más de 8 caracteres o no coinciden los dos campos*** ".'</br>';
        $cumpleCondicion = false;
    }

    if($cumpleCondicion){
        echo " Todos los campos son Válidos ";
    }
    return $mensajeError;
}