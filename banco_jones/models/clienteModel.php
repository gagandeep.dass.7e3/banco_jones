<?php
require_once('../db/DBManager.php');
use DBManager;

function insertCliente($cliente){

    $manager = new DBManager();
    try{
        $sql="INSERT INTO cliente (nombre, apellidos, fecha_nacimiento, sexo, telefono, dni, email, password) VALUES (:nombre,:apellidos,:fecha_nacimiento,:sexo,:telefono,:dni,:email,:password)";

        $password=password_hash($cliente->getPassword(),PASSWORD_DEFAULT,['cost'=>10]);

        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':nombre',$cliente->getNombre());
        $stmt->bindParam(':apellidos',$cliente->getApellidos());
        $stmt->bindParam(':edad',$cliente->getFechaNacimiento());
        $stmt->bindParam(':sexo',$cliente->getSexo());
        $stmt->bindParam(':telefono',$cliente->getTelefono());
        $stmt->bindParam(':dni',$cliente->getDni());
        $stmt->bindParam(':email',$cliente->getEmail());
        $stmt->bindParam(':contrasenya',$password);

        if ($stmt->execute()){
            echo "todo OK";
        }else{
            echo "MAL";
        }

    }catch (PDOException $e){
        echo $e->getMessage();
    }


}
