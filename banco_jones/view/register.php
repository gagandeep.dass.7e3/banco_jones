<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles.css">
    <title>Registro</title>
</head>

<body>
<h1>Register</h1>
<form action="../controller/controller.php" method="post">
    <div>
        <label for="name">Nombre:</label>
        <input name="name" type="text" value="<?php if ( isset($_POST['name'])) echo $_POST['name']?>">
    </div>

    <div>
        <label for="apellido">Apellidos:</label>
        <input name="apellido" type="text" value="<?php if ( isset($_POST['surname'])) echo $_POST['surname']?>">
    </div>

    <div>
        <label for="genero">Género:</label>
        <select name="genre">
            <option selected>Selecciona tu género</option>
            <option value="1">Hombre</option>
            <option value="2">Mujer</option>
        </select>
    </div>

    <div>
        <label for="birthdate">Fecha de nacimiento:</label>
        <input name="birthdate" type="date" value="<?php if ( isset($_POST['birthdate'])) echo $_POST['birthdate']?>">
    </div>

    <div>
        <label for="dni">DNI:</label>
        <input name="dni" type="text" value="<?php if ( isset($_POST['dni'])) echo $_POST['dni']?>">
    </div>

    <div>
        <label for="mobileNumber">Numero de teléfono:</label>
        <input name="mobilePhone" type="tel" value="<?php if ( isset($_POST['mobilePhone'])) echo $_POST['mobilePhone']?>">
    </div>

    <div>
        <label for="email">Email:</label>
        <input name="email" type="email" value="<?php if ( isset($_POST['email'])) echo $_POST['email']?>">
    </div>

    <div>
        <label for="password">Contraseña:</label>
        <input name="password" type="password"  value="<?php if ( isset($_POST['pass'])) echo $_POST['pass']?>">
    </div>

    <div>
        <label for="repeatPassword">Contraseña:</label>
        <input name="repeatPassword" type="password" value="<?php if ( isset($_POST['pass'])) echo $_POST['pass']?>">
    </div>

    <div>
        <input type="hidden" value="register" name="control">
    </div>

    <div>
        <input type="submit" name="submit" value="Registrar">
    </div>
</form>

<?php
if (isset($_POST['message']))
    echo $_POST['message'] . '<br/>';
?>
</body>

</html>