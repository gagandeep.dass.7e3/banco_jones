<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../styles.css">
    <title>Logout</title>
</head>

<body>
<h1>Logout</h1>
<form action="../controller/controller.php" method="post">
    <p>Tu sesion se ha cerrado</p>
    <p>Adiós!</p>
</form>
</body>

</body>
</html>