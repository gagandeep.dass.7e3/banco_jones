<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../styles.css">
    <title>Query</title>
</head>

<body>
<h1>Query</h1>
<nav>
    <a href="profile.php">Perfil</a>
    <a href="transfer.php">Transferencia</a>
    <a href="logout.php">Logout</a>
</nav>
<form action="../controller/controller.php" method="post">
    <div>
        <label for="pass">Transacciones:</label>
        <ol>
            <li>Transaccion</li>
            <li>AMZN Mktp ES</li>
            <li>Metro Barcelona P</li>
            <li>Paypal</li>
        </ol>
    </div>
    <p>Puedes filtrar las transacciones:</p>
    <select>
        <option selected>Escoje opción</option>
        <option value="1">Todas</option>
        <option value="2">Recibidas</option>
        <option value="3">Enviadas</option>
    </select>
    <p><a href="init.php">Volver a Init</a></p>
</form>
</body>
</html>