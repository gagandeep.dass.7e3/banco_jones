<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../styles.css">
    <title>Transferencia</title>
</head>

<body>
<h1>Transferencia</h1>
<nav class="nav">
    <a href="profile.php">Perfil</a>
    <a href="init.php">Init</a>
    <a href="transfer.php">Transferencia</a>
    <a href="logout.php">Logout</a>
</nav>
<form action="../controller/controller.php" method="post">
    <div>
        <label>IBAN:</label>
        <input type="text">
    </div>

    <div>
        <label>Cantidad:</label>
        <input type="text">
    </div>

    <div>
        <label>Concepto:</label>
        <textarea placeholder="Factura de luz"></textarea>
    </div>

    <div>
        <input type="hidden" value="transfer" name="control">
    </div>
    <div>
        <input type="submit" name="submit" value="Enviar">
    </div>
</form>
</body>
</html>