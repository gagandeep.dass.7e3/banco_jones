<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../styles.css">
    <title>Login</title>
</head>

<body>
<h1>Login</h1>
<form action="../controller/controller.php" method="post" >
    <div>
        <label for="name">Nombre:</label>
        <input name="name" type="text">
    </div>

    <div>
        <label for="password">Contraseña:</label>
        <input name="pass" type="password">
    </div>

    <div>
        <input type="hidden" value="login" name="control">
    </div>

    <div>
        <input type="submit" name="submit" value="Acceder">
    </div>

</form>
<?php
if (isset($_POST['message']))
    echo $_POST['message'] . '<br/>';
?>
</body>
</html>